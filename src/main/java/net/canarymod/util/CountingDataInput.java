/*
 * Copyright (c) 2015 LexManos/MinecraftForgeTeam
 * All rights reserved.
 */

package net.canarymod.util;

import net.minecraft.nbt.NBTSizeTracker;

import java.io.DataInput;
import java.io.IOException;

/**
 * This is a wrapper around a DataInput and a NBTSizeTracker, this is a simple
 * way to TRUELY keep track of everything that we are reading from the buffer.
 */
public class CountingDataInput implements DataInput {
    private DataInput wrapped;
    private NBTSizeTracker tracker;

    public CountingDataInput(DataInput wrapped, NBTSizeTracker tracker) {
        this.wrapped = wrapped;
        this.tracker = tracker;
    }

    @Override
    public boolean readBoolean() throws IOException {
        tracker.a(8);
        return wrapped.readBoolean();
    }

    @Override
    public byte readByte() throws IOException {
        tracker.a(8);
        return wrapped.readByte();
    }

    @Override
    public char readChar() throws IOException {
        tracker.a(16);
        return wrapped.readChar();
    }

    @Override
    public double readDouble() throws IOException {
        tracker.a(64);
        return wrapped.readDouble();
    }

    @Override
    public float readFloat() throws IOException {
        tracker.a(32);
        return wrapped.readFloat();
    }

    @Override
    public void readFully(byte[] b) throws IOException {
        tracker.a(8 * b.length);
        wrapped.readFully(b);
    }

    @Override
    public void readFully(byte[] b, int off, int len) throws IOException {
        tracker.a(8 * len);
        wrapped.readFully(b, off, len);
    }

    @Override
    public int readInt() throws IOException {
        tracker.a(32);
        return wrapped.readInt();
    }

    @Override
    public String readLine() throws IOException {
        String ret = wrapped.readLine();

        if (ret == null)
            return null;

        tracker.a(8 * ret.length());
        return ret;
    }

    @Override
    public long readLong() throws IOException {
        tracker.a(64);
        return wrapped.readLong();
    }

    @Override
    public short readShort() throws IOException {
        tracker.a(16);
        return wrapped.readShort();
    }

    @Override
    public String readUTF() throws IOException {
        String ret = wrapped.readUTF();
        NBTSizeTracker.readUTF(tracker, ret);
        return ret;
    }

    @Override
    public int readUnsignedByte() throws IOException {
        tracker.a(8);
        return wrapped.readUnsignedByte();
    }

    @Override
    public int readUnsignedShort() throws IOException {
        tracker.a(16);
        return wrapped.readUnsignedShort();
    }

    @Override
    public int skipBytes(int n) throws IOException {
        //Technically this skips, but lets count it in the size as well.
        // This is never used in vanilla NBT parsing, but just in case!
        tracker.a(8 * n);
        return wrapped.skipBytes(n);
    }

}
