package net.canarymod;

import java.util.List;

import net.canarymod.config.Configuration;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.entity.passive.EntityAmbientCreature;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.boss.EntityDragonPart;
import net.minecraft.entity.item.EntityEnderCrystal;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraft.entity.item.EntityFireworkRocket;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.effect.EntityWeatherEffect;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.util.MathHelper;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;

public class ActivationRange {

    static AxisAlignedBB maxBB = AxisAlignedBB.a(0, 0, 0, 0, 0, 0);
    static AxisAlignedBB miscBB = AxisAlignedBB.a(0, 0, 0, 0, 0, 0);
    static AxisAlignedBB animalBB = AxisAlignedBB.a(0, 0, 0, 0, 0, 0);
    static AxisAlignedBB monsterBB = AxisAlignedBB.a(0, 0, 0, 0, 0, 0);

    /**
     * Initalizes an entities type on construction to specify what group this
     * entity is in for activation ranges.
     *
     * @param entity
     * @return group id
     */
    public static byte initializeEntityActivationType(Entity entity)
    {
        if(entity instanceof EntityMob || entity instanceof EntitySlime)
        {
            return 1; // Monster
        } else if(entity instanceof EntityCreature || entity instanceof EntityAmbientCreature)
        {
            return 2; // Animal
        } else
        {
            return 3; // Misc
        }
    }

    /**
     * These entities are excluded from activation range checks.
     *
     * @param entity
     * @param //world
     * @return boolean If it should always tick.
     */
    public static boolean initializeEntityActivationState(Entity entity)
    {
        if((entity.activationType == 3 && Configuration.getServerConfig().miscActivationRange() == 0)
                ||(entity.activationType == 2 && Configuration.getServerConfig().animalActivationRange() == 0)
                ||(entity.activationType == 1 && Configuration.getServerConfig().monsterActivationRange() == 0)
                ||entity instanceof EntityPlayer
                ||entity instanceof EntityThrowable
                ||entity instanceof EntityDragonPart
                ||entity instanceof EntityWither
                ||entity instanceof EntityDragon
                ||entity instanceof EntityFireball
                ||entity instanceof EntityWeatherEffect
                ||entity instanceof EntityTNTPrimed
                ||entity instanceof EntityEnderCrystal
                ||entity instanceof EntityFireworkRocket)
        {
            return true;
        }

        return false;
    }

    /**
     * Utility method to grow an AABB without creating a new AABB or touching
     * the pool, so we can re-use ones we have.
     *
     * @param target
     * @param source
     * @param x
     * @param y
     * @param z
     */
    public static void growBB(AxisAlignedBB target, AxisAlignedBB source, int x, int y, int z)
    {
        target.a = source.a - x;
        target.b = source.b - y;
        target.c = source.c - z;
        target.d = source.d + x;
        target.e = source.e + y;
        target.f = source.f + z;
    }

    /**
     * Find what entities are in range of the players in the world and set
     * active if in range
     *
     * @param world
     */
    public static void activateEntities(World world)
    {
        final int miscActivationRange = Configuration.getServerConfig().miscActivationRange();
        final int animalActivationRange = Configuration.getServerConfig().animalActivationRange();
        final int monsterActivationRange = Configuration.getServerConfig().monsterActivationRange();

        int maxRange = Math.max(monsterActivationRange, animalActivationRange);
        maxRange = Math.max(maxRange, miscActivationRange);
        maxRange = Math.min((Configuration.getServerConfig().getViewDistance() << 4) - 8, maxRange);

        for(Entity player : (List<Entity>) world.h)
        {
            player.activatedTick = MinecraftServer.I().al();
            growBB(maxBB, player.C, maxRange, 256, maxRange);
            growBB(miscBB, player.C, maxRange, 256, maxRange);
            growBB(animalBB, player.C, maxRange, 256, maxRange);
            growBB(monsterBB, player.C, maxRange, 256, maxRange);

            int i = MathHelper.c(maxBB.a / 16.0D);
            int j = MathHelper.c(maxBB.d / 16.0D);
            int k = MathHelper.c(maxBB.c / 16.0D);
            int l = MathHelper.c(maxBB.f / 16.0D);

            for(int i1 = i; i1 <= j; ++i1)
            {
                for(int j1 = k; j1 <= l; ++j1)
                {
                    if(world.c(i1, j1));
                    {
                        activateChunkEntities(world.e(i1, j1));
                    }
                }
            }
        }
    }

    /**
     * Checks for the activation state of all entities in this chunk.
     *
     * @param chunk
     */
    private static void activateChunkEntities(Chunk chunk)
    {
        for(List<Entity> slice : chunk.j)
        {
            for(Entity entity : slice)
            {
                if(MinecraftServer.I().al() > entity.activatedTick)
                {
                    if(entity.defaultActivationState)
                    {
                        entity.activatedTick = MinecraftServer.I().al();
                        continue;
                    }
                    switch(entity.activationType) {
                        case 1:
                            if (monsterBB.b(entity.C)) {
                                entity.activatedTick = MinecraftServer.I().al();
                            }
                            break;
                        case 2:
                            if (animalBB.b(entity.C)) {
                                entity.activatedTick = MinecraftServer.I().al();
                            }
                            break;
                        case 3:
                        default:
                            if (miscBB.b(entity.C)) {
                                entity.activatedTick = MinecraftServer.I().al();
                            }
                    }
                }
            }
        }
    }

    /**
     * If an entity is not in range, do some more checks to see if we should
     * give it a shot
     *
     * @param entity
     * @return
     */
    public static boolean checkEntityImmunities(Entity entity) {
        // quick checks
        if (entity.ac || entity.e > 0) {
            return true;
        }
        if (!(entity instanceof EntityArrow)) {
            if (!entity.D || entity.l != null || entity.m != null) {
                return true;
            }
        } else if (!((EntityArrow) entity).D)
        {
            return true;
        }
        // special cases.
        if(entity instanceof EntityLivingBase)
        {
            EntityLivingBase living = (EntityLivingBase) entity;
            if(living.aB > 0 || living.ax > 0 || living.f.size() > 0)
            {
                return true;
            }
            if(entity instanceof EntityCreature && ((EntityCreature)entity).bm != null)
            {
                return true;
            }
            if(entity instanceof EntityVillager && ((EntityVillager)entity).bY())
            {
                return true;
            }
            if(entity instanceof EntityAnimal)
            {
                EntityAnimal animal = (EntityAnimal) entity;
                if(animal.f() || animal.ce())
                {
                    return true;
                }
                if(entity instanceof EntitySheep && ((EntitySheep)entity).ca())
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if the entity is active for this tick.
     *
     * @param entity
     * @return
     */
    public static boolean checkIfActive(Entity entity) {
        boolean isActive = entity.activatedTick >= MinecraftServer.I().al() || entity.defaultActivationState;

        // Should this entity tick?
        if (!isActive) {
            if ((MinecraftServer.I().al() - entity.activatedTick - 1) % 20 == 0) {
                // Check immunities every 20 ticks.
                if (checkEntityImmunities(entity)) {
                    entity.activatedTick = MinecraftServer.I().al() + 20;
                }
                isActive = true;
            }
            // Add a little performance juice to active entities. Skip 1/4 if not immune.
        } else if (!entity.defaultActivationState && entity.aa % 4 == 0 && !checkEntityImmunities(entity))
        {
            isActive = false;
        }
        int x = MathHelper.c(entity.s);
        int z = MathHelper.c(entity.u);
        // Make sure not on edge of unloaded chunk.
        if(isActive && !entity.o.a(x, 0, z, 16))
        {
            isActive = false;
        }
        return isActive;
    }
}
